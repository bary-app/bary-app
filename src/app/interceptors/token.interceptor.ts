import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { UserService } from '../services/index';
import { Observable } from 'rxjs/Observable';
import { API_URL } from '../app.values';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private EXCEPT:any = [
  "user/login",
  "user/signup",
  "user/recovery"
  ];

  constructor(public user: UserService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if(this.user.isLogged() && !this.EXCEPT.includes(API_URL+request.url)){
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.user.token}`
        }
      });
    }

    return next.handle(request);
  }
}
