import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { routing, routingProviders } from './app.routing';
import { DropdownComponent } from './_dropdown/dropdown.component';

import { ModalModule } from './modals/modal.module';
import { SharedModule } from './_shared_module/shared.module';

import { AuthGuard } from './guards/auth.guard';

import { HeaderComponent } from './_header/header.component';
import { NavComponent } from './_nav/nav.component';
import { SearchBarComponent } from './_nav/_search_bar/search_bar.component';
import { SearchBarBoxComponent } from './_nav/_search_bar/_search_bar_box/search_bar_box.component';
import { CitySelectorComponent } from './_header/_city_selector/city_selector.component';
import { RecentsSearchService } from './services/recents_search.service';

import * as modals from './modals/index';
import * as pages from './pages/index';
import * as services from './services/index';
import * as interceptors from './interceptors/index';

import { OWL_DATE_TIME_LOCALE} from 'ng-pick-datetime';

@NgModule({
	declarations: [
	AppComponent,
	DropdownComponent,
	HeaderComponent,
	NavComponent,
	CitySelectorComponent,
	SearchBarComponent,
	SearchBarBoxComponent,
	modals.LoginComponent,
	modals.RecoverPasswordComponent,
	modals.SignupComponent,
	pages.ErrorComponent,
	],
	imports: [
	SharedModule,
	BrowserModule,
	HttpClientModule,
	ReactiveFormsModule,
	SimpleNotificationsModule.forRoot(),
	FormsModule,
	ModalModule,
	routing,
	pages.DiscoverModule,
	pages.SearchModule,
	pages.RestaurantModule,
	pages.MeModule,
	pages.SettingsModule
	],
	providers: [routingProviders, services.LoaderService, services.UserService, services.RestaurantService, services.AuthService, AuthGuard, RecentsSearchService, {provide: OWL_DATE_TIME_LOCALE, useValue: 'es'}, {provide: APP_INITIALIZER,
		useFactory: (userServ: services.AuthService) => () => userServ.load_info(),
		deps: [services.AuthService, services.UserService, HttpClient, services.AuthService],
		multi: true
	},
	{
		provide: HTTP_INTERCEPTORS,
		useClass: interceptors.TokenInterceptor,
		multi: true
	},
	{
		provide: HTTP_INTERCEPTORS,
		useClass: interceptors.InvalidTokenInterceptor,
		multi: true
	}],
	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
