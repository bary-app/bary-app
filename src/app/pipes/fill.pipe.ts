import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'fill'
})
export class FillPipe implements PipeTransform {
	transform(value: number): any {
		const iterable = {};
		iterable[Symbol.iterator] = function* () {
			let n = 0;
			while (n < Math.trunc(value)) {
				yield ++n;
			}
		};
		return iterable;
	}
}