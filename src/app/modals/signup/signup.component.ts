import { ModalComponent } from '../modal.component';
import { Component } from '@angular/core';

import { AuthService } from '../../services/index';

@Component({
	selector: 'signup-modal',
	templateUrl: './signup.component.pug'
})

export class SignupComponent extends ModalComponent{
	private loading:boolean = false;
	private error:string = null;

	constructor(private auth:AuthService){super()}

	public signup(data): void {		
		if(!this.loading) 
			if(data.status == "VALID"){
				this.error = null;
				this.loading = true;

				var user = data.form.value;
				
				this.auth.signup(user).subscribe(res => {					
					if(res.error){
						this.loading = false;
						this.error= res.error;
					}
				});

			}else this.error = 'Datos no validos o no introducidos!';
		}

	}