import { Component,ChangeDetectorRef, ViewChild } from '@angular/core';

import { NotificationsService } from 'angular2-notifications';
import { LoaderService, UserService, AuthService } from './services/index';

import { LoginComponent, RecoverPasswordComponent,SignupComponent } from './modals/index';

@Component({
	selector: 'body',
	templateUrl: './app.component.pug',
	styleUrls: ['./app.component.scss']
})

export class AppComponent {
	public instance: AppComponent;
	private showLoader: boolean;

	@ViewChild('login') loginModal: LoginComponent;
	@ViewChild('recover') recoverModal: RecoverPasswordComponent;
	@ViewChild('signup') signupModal: SignupComponent;

	private isEmailConfirmed:boolean=true;
	private notification_options:any = {
		"timeOut": 5000,
		"showProgressBar": false
	};

	constructor(
		private Cdr: ChangeDetectorRef,
		private Loader: LoaderService,
		private User: UserService,
		private authService:AuthService,
		private notificationService: NotificationsService
		) {
		document.querySelector('body').removeAttribute('class');
		this.instance = this;
	}

	ngAfterViewInit() {
		this.Loader.status.subscribe((val: boolean) => {
			this.showLoader = val;
			this.Cdr.detectChanges();
		});

		if(this.User.info)
			this.User.info.subscription.subscribe((val: any) => {
				this.isEmailConfirmed = val.isConfirmed;
				this.Cdr.detectChanges();
			});
	}

	private loading_re_petition:boolean = false;
	private resend_email():void{
		if(!this.loading_re_petition){
			this.loading_re_petition = true;
			this.Loader.display(true);

			this.authService.resendEmailConfirmation().subscribe(data => {
				this.loading_re_petition = false;
				this.Loader.display(false);

				this.notificationService.success("Email enviado!", "Se le envió un correo para confirmar su cuenta.", false);
			}, error => {
				this.loading_re_petition = false;
				this.Loader.display(false);
				
				this.notificationService.error("Error", "Error al enviar el correo de confirmación.", false);
			});
		}
	}
}
