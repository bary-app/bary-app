import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { LoaderService, AuthService } from '../../../services/index';

import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class ProfileResolver implements Resolve<Observable<any>> {	
	constructor(
		private authService: AuthService, 
		private loaderService: LoaderService
		) {}

	resolve(route: ActivatedRouteSnapshot) {

		this.loaderService.display(true);

		return this.authService.profile().do(data => {
			this.loaderService.display(false);
		}).catch((e) => {
			this.loaderService.display(false);

			return Observable.of({});
		});
	}
}