import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
	selector: 'profile-component',
	templateUrl: './profile.component.pug',
	styleUrls: ['./profile.component.scss']
})

export class MeProfileComponent implements OnInit{
	private data:any;

	constructor (
		private activatedRoute:ActivatedRoute
		){}

	ngOnInit() {
		this.data = this.activatedRoute.snapshot.data.profile;
	}

	private hasPassed(date): boolean{
		return moment(date).isBefore(moment());
	}
}
