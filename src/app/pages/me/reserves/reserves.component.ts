import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { UserService, LoaderService } from '../../../services/index';
import { ReservesService } from './reserves.service';

import * as moment from 'moment';

@Component({
	selector: 'reserves-component',
	templateUrl: './reserves.component.pug',
	styleUrls: ['./reserves.component.scss']
})

export class MeReservesComponent implements OnInit{
	private page:number = 1;
	private reservesData: any;

	private currentReserve:any;

	@ViewChild('cancelConfirm') cancelConfirmModal;
	@ViewChild('editModal') editModal;

	constructor (
		private activatedRoute:ActivatedRoute,
		private router: Router,
		private loaderService: LoaderService,
		private notificationsService: NotificationsService,
		private userService: UserService,
		private reservesService: ReservesService
		){}

	ngOnInit() {
		this.reservesData = this.activatedRoute.snapshot.data.reserves;
	}

	private parseDate(date: string): string{
		let new_date = date.replace(' ', 'T').replace('/', '-');
		console.log(new_date);
		return new_date;
	}

	private hasNotPassed(date): boolean{
		return moment(moment()).isBefore(date);
	}

	private edit(e: MouseEvent, reserve:any){
		e.stopPropagation();

		this.currentReserve = reserve;
		this.editModal.show();

		return false;
	}

	private remove(e: MouseEvent, reserve:any){
		e.stopPropagation();

		this.currentReserve = reserve;
		this.cancelConfirmModal.show();

		return false;
	}

	private modalOnClose(res:boolean): void{
		if(res){
			this.loaderService.display(true);
			this.reservesService.cancel(this.currentReserve.code).subscribe(res => {
				this.loaderService.display(false);
				if(res.status == 1){
					let pos = this.reservesData.coming.indexOf(this.currentReserve);
					if (pos !== -1){
						this.currentReserve.state = "CANCELED";
						this.reservesData.others.data.unshift(this.currentReserve);
						this.reservesData.coming.splice(pos, 1);
					}

					this.notificationsService.success("Genial!", "Reserva cancelada correctamente.", false);
				}

				this.currentReserve = undefined;
			}, err => {
				console.error(err);
				this.loaderService.display(false);
				this.currentReserve = undefined;

				this.notificationsService.error("Error", "No se ha podido cancelar su reserva.", false);
			});
		}	
	}

	onResultsScroll(){
		if(this.reservesData.others && this.reservesData.others.pages > this.page){
			this.page++;
			this.loaderService.display(true);

			this.reservesService.get(this.page).subscribe(res => {
				for (let restaurant_pos in res.others.data) {
					this.reservesData.others.data.push(res.others.data[restaurant_pos]);
				}
				this.loaderService.display(false);
			}, error => {
				this.page--;
			});
		}
	}
}
