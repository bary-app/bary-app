import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../guards/auth.guard';

import { MeComponent } from './me.component';
import { MeProfileComponent } from './profile/profile.component';
import { MeReservesComponent } from './reserves/reserves.component';
import { MeFavouritesComponent } from './favourites/favourites.component';

import { ProfileResolver } from './profile/profile.resolver';
import { ReservesResolver } from './reserves/reserves.resolver';
import { FavouritesResolver } from './favourites/favourites.resolver';

const myRoutes: Routes = [
{
	path: 'me',
	component: MeComponent,
	children: [
	{
		path: '',
		component: MeProfileComponent,
		canActivate: [ AuthGuard ],
		resolve: { profile:  ProfileResolver },
	},
	{
		path: 'reserves',
		component: MeReservesComponent,
		resolve: { reserves:  ReservesResolver },
	},
	{
		path: 'favourites',
		component: MeFavouritesComponent,
		canActivate: [ AuthGuard ],
		resolve: { favourites: FavouritesResolver },
	}
	]
}
];

@NgModule({
	imports: [
	RouterModule.forChild(myRoutes)
	],
	exports: [
	RouterModule
	]
})

export class MeRoutingModule { }