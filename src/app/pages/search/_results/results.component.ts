import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../../services/loader.service';
import { SearchService } from '../search.service';

@Component({
	selector: '.results',
	templateUrl: './results.component.pug'
})
export class ResultsComponent{
	@Input()
	page:number;

	@Input()
	query:string;

	@Input()
	reserve:any;

	private results:any;

	constructor(private activatedRoute:ActivatedRoute, private searchService: SearchService, private loaderService: LoaderService){}

	ngOnInit(){
		this.activatedRoute.data.subscribe(data => {
			this.results = data.search_results;

			console.log(this.results);
		});
	}

	get_params(page:number):any{
		let params:any = {};

		if(this.query)
			params.q = this.query;

		if(this.reserve.date && this.reserve.pers){
			params.date = this.reserve.date;
			params.pers = this.reserve.pers;
		}

		params.p = page;

		return params;
	}

	onResultsScroll(){
		if(this.results && this.results.restaurants.pages > this.page){
			this.page++;
			this.loaderService.display(true);

			this.searchService.get(this.page, this.query, this.reserve).subscribe(res => {
				for (let restaurant_pos in res.restaurants.data) {
					this.results.restaurants.data.push(res.restaurants.data[restaurant_pos]);
				}
				this.loaderService.display(false);
			}, error => {
				this.page--;
			});
		}
	}
}	