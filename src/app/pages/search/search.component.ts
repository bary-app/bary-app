import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecentsSearchService } from '../../services/recents_search.service';

@Component({
	selector: 'main.search',
	templateUrl: './search.component.pug'
})
export class SearchComponent{
	private sub:any;

	private q:string;
	private reserve:any;
	private p:number;

	constructor (private activatedRoute:ActivatedRoute, private recents_search:RecentsSearchService){}

	ngOnInit() {
		this.sub = this.activatedRoute.params.subscribe(params => { 
			this.q = params['q'];
			this.reserve = {
				"date": params['date'],
				"pers": (+params['pers'] || 0)
			}
			this.p = (+params['p'] || 1);
			
			if(this.q && this.q.trim() != "")
				this.recents_search.add(this.q);
		});
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}
}
