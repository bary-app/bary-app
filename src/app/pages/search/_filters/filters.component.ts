import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment'

@Component({
	selector: '.filters',
	templateUrl: './filters.component.pug'
})

export class FiltersComponent {
	private _reserve:any;

	@Input()
	set reserve(value: any) {
		this._reserve = Object.assign({}, value);
		if(this._reserve.date)
			this._reserve.date = moment(this._reserve.date).toDate();
	}

	@Input()
	q:string;

	private min_date:Date;
	private max_date:Date;

	constructor(private router:Router){}

	ngOnInit(){
		var minutes = new Date().getMinutes() - (new Date().getMinutes()%30)+30;
		var date = new Date();
		date.setMinutes(minutes);

		this.min_date = date;

		let max = new Date();
		max.setMonth(max.getMonth() + 3);
		max.setHours(23);
		max.setMinutes(30);
		this.max_date = max;
	}

	private emit(){
		this.goSearch();
	}

	private goSearch(){
		let params:any = {q: this.q};

		if(this._reserve.date && this._reserve.pers > 0){
			params.date = moment(this._reserve.date).seconds(0).format('YYYY-MM-DD HH:mm:ss'); 
			params.pers = this._reserve.pers;
		}

		this.router.navigate(['./search', params]);
	}
}