import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'all-component',
	templateUrl: './all.component.pug'
})

export class DiscoverAllComponent implements OnInit{
	public restaurants:any[] = [];

	constructor (private activatedRoute:ActivatedRoute){
	}

	ngOnInit() {
		this.restaurants = this.activatedRoute.snapshot.data.restaurants;
	}
}
