import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CitiesService } from '../../services/index';
import { API_URL } from '../../app.values';

@Injectable()
export class DiscoverService{

	constructor(private http:HttpClient){}

	public get():Observable<any>{
		return this.http.get(API_URL+'c/'+CitiesService.getCity().id+'/discover', {});
	}
}