export { DiscoverModule } from './discover/discover.module';
export { SearchModule } from './search/search.module';
export { RestaurantModule } from './restaurant/restaurant.module';
export { MeModule } from './me/me.module';
export { SettingsModule } from './settings/settings.module';
export { ErrorComponent } from './error/error.component';