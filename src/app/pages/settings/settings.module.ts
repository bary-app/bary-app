import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsProfileComponent } from './profile/profile.component';
import { SettingsAccountComponent } from './account/account.component';
import { TypePasswordComponent } from './account/type_password/type_password.component';

import { SettingsService } from './settings.service';
import { ProfileResolver } from './profile/profile.resolver';

@NgModule({
	declarations: [
	SettingsComponent,
	SettingsProfileComponent,
	SettingsAccountComponent,
	TypePasswordComponent
	],
	imports: [
	BrowserModule,
	FormsModule,
	SettingsRoutingModule
	],
	providers: [SettingsService, ProfileResolver],
	bootstrap: [SettingsComponent]
})
export class SettingsModule { }
