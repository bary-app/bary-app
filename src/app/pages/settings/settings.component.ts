import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../services/index';

@Component({
	selector: 'main',
	templateUrl: './settings.component.pug'
})

export class SettingsComponent{

	constructor (private loaderService: LoaderService){
	}

	ngOnInit() {
		//http call starts
		this.loaderService.display(true);
		this.loaderService.display(false);
		//http call ends
	}
}
