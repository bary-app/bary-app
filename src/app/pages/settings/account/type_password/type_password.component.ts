import { ModalComponent } from '../../../../modals/modal.component';
import { Component } from '@angular/core';

@Component({
	selector: 'type-password-modal',
	templateUrl: './type_password.component.pug',
	styleUrls: ['./type_password.component.scss']
})
export class TypePasswordComponent extends ModalComponent{
	private cb:any;

	constructor(){super()}

	open(cb: any): void{
		this.show();
		this.cb = cb;
	}

	private run(form): void{
		let password = form.value.password;

		this.cb(password);
		form.reset();
		
		super.hide();
	}
}