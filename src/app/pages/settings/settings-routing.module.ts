import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../guards/auth.guard';

import { SettingsComponent } from './settings.component';
import { SettingsProfileComponent } from './profile/profile.component';
import { SettingsAccountComponent } from './account/account.component';

import { ProfileResolver } from './profile/profile.resolver';

const discoverRoutes: Routes = [
{
  path: 'settings',
  component: SettingsComponent,
  children: [
  {
    path: '',
    component: SettingsProfileComponent,
    canActivate: [ AuthGuard ],
    resolve: { profile:  ProfileResolver },
  },
  {
    path: 'account',
    component: SettingsAccountComponent
  }
  ]
}
];

@NgModule({
  imports: [
  RouterModule.forChild(discoverRoutes)
  ],
  exports: [
  RouterModule
  ]
})

export class SettingsRoutingModule { }