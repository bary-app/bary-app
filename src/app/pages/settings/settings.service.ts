import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

import { API_URL } from '../../app.values';

@Injectable()
export class SettingsService{

	constructor(private http:HttpClient){}

	public get():Observable<any>{
		return this.http.get(API_URL+'user/settings', {});
	}

	public save(user: any):Observable<any>{
		return this.http.post(API_URL+'user/settings', user);
	}

	public change_email(data: any):Observable<any>{
		return this.http.post(API_URL+'user/settings/email', data);
	}

	public change_password(data: any):Observable<any>{
		return this.http.post(API_URL+'user/settings/password', data);
	}

	public delete_account(data: any):Observable<any>{
		return this.http.post(API_URL+'user/settings/delete_account', data);
	}
}