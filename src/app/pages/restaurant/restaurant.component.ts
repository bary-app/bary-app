import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';

import { RestaurantService, CitiesService } from '../../services/index';
import { RestaurantReserveComponent } from './_reserve/reserve.component';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'main',
	templateUrl: './restaurant.component.pug',
	styleUrls: ['./restaurant.component.scss']
})

export class RestaurantComponent implements OnInit, OnDestroy{
	public restaurant:any;
	private dataSubscription:any;

	private isAtSameCity:boolean = true;

	private _reloadReserves:boolean = false;
	private loadingFav:boolean = false;

	private aside_isSticky:boolean = false;

	constructor (private elementRef:ElementRef, private route: ActivatedRoute, private restaurantService: RestaurantService){}

	private stick(){
		window.addEventListener('scroll', (e) => {
			if (window.pageYOffset > 60) {
				this.aside_isSticky = true;
			} else {
				this.aside_isSticky = false;
			}
		});
	}

	ngOnInit() {
		this.stick();
		this.loadingFav = false;

		this.dataSubscription = this.route.data.subscribe(data => {
			this.restaurant = data.restaurant;

			this.isAtSameCity = (CitiesService.getCity().id == this.restaurant.city.id);

			// Forzar recarga del componente de reservas
			this._reloadReserves = false;
			setTimeout(function () {
				this._reloadReserves = true;
			}.bind(this),100);
		});
	}

	ngOnDestroy(){
		if (this.dataSubscription)
			this.dataSubscription.unsubscribe();
	}

	addToFavourites(){
		if(this.restaurant.is_favourite !== null){
			this.loadingFav = true;

			this.restaurantService.addToFavourites(this.restaurant.id, !this.restaurant.is_favourite).subscribe(res => {
				this.restaurant.is_favourite = !this.restaurant.is_favourite;
				this.loadingFav = false;
			}, error => {
				console.error(error);
				this.loadingFav = false;
			});
		}
	}

	goToDiv(fragment: string): void {
		window.location.hash = fragment;
	}
}
