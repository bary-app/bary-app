import { Component, Input } from '@angular/core';

@Component({
	selector: '.photo-slider',
	templateUrl: './photo-slider.component.pug',
	styleUrls: ['./photo-slider.component.scss']
})

export class RestaurantPhotoSliderComponent{
	@Input()
	private photos:any[];

	private i:number;

	constructor(){
		this.i = 0;
	}

	goTo(num:number){
		setTimeout(function(){
			this.i = num;
		}.bind(this), 250);
	}
}