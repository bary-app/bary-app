import { Injectable } from '@angular/core';

@Injectable()
export class City {

	private _id:Number;
	private _name:String;
	private _country:String;

	constructor(
		id:Number,
		name:String,
		country:String
		){
		this._id = id;
		this._name = name;
		this._country = country
	}

	get id():Number{
		return this._id;
	}

	get name():String{
		return this._name;
	}

	get country():String{
		return this._country;
	}
}