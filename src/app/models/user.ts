import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class User {

	public subscription: BehaviorSubject<User>;

	constructor(
		private _name:string,
		private _surname:string,
		private _confirmed_email:boolean
		){
		this.subscription = new BehaviorSubject<User>(this);
	}

	get name():string{
		return this._name;
	}

	get surname():string{
		return this._surname;
	}

	get isConfirmed():boolean{
		return this._confirmed_email;
	}

	set isConfirmed(confirmed){
		this._confirmed_email = confirmed;
	}

	public save():void{
		let data = Object.assign({},this);
		delete data.subscription;

		sessionStorage.setItem('user.data', JSON.stringify(data));

		this.subscription.next(this);
	}
}