import { Component, HostBinding } from '@angular/core';

@Component({
	selector: 'nav',
	templateUrl: './nav.component.pug',
	styleUrls: ['./nav.component.scss']
})

export class NavComponent {

	@HostBinding('class.sticky') isSticky: boolean = false;

	private showNav:boolean = false;

	private stick(){
		window.addEventListener('scroll', (e) => {
			if (window.pageYOffset >= 45 && !this.isSticky) 
				this.isSticky = true;
			else if(window.pageYOffset < 45 && this.isSticky)
				this.isSticky = false;
		});
	}

	constructor() {
		this.stick();
	}
}