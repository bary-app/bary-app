 import { Component, OnInit, HostBinding, ElementRef } from '@angular/core';
 import { SearchBarBoxComponent} from './_search_bar_box/search_bar_box.component';
 import { Router, RoutesRecognized } from '@angular/router';

 @Component({
 	selector: '.search-bar',
 	templateUrl: './search_bar.component.pug',
 	styleUrls: ['./search_bar.component.scss'],
 	host: {
 		'(document:click)': 'onClick($event)',
 	}
 })

 export class SearchBarComponent {
 	public loaded:boolean;

 	private search:string;

 	@HostBinding('class.focus') isFocus: boolean = false;

 	constructor (private router: Router, private _el: ElementRef){}

 	ngOnInit(){
 		this.loaded = true;
 		this.search = "";

 		this.router.events.subscribe(val => {
 			if (val instanceof RoutesRecognized) {
 				let params = val.state.root.firstChild.params;

 				if(params.q)
 					this.search = params.q;
 			}
 		});
 	}

 	private updateSearch(val){
 		this.search = val;
 		this.goSearch();
 	}

 	private updateLoaded(val){
 		this.loaded = val;
 	}

 	private onClick(event){
 		if (!this._el.nativeElement.contains(event.target) || event.target.tagName == 'A' || event.target.parentElement.className == 'search-list' || event.target.parentElement.parentElement.className == 'search-list' || event.target.parentElement.parentElement.parentElement.className == 'search-list')
 			this.add_focus(false);
 	}

 	public add_focus(yes:boolean){
 		this.isFocus=yes;
 	}

 	private goSearch(){
 		this.add_focus(false);

 		this.router.navigate(['./search', { q: this.search }]);
 	}
 }