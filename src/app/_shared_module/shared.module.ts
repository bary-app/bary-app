import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FillPipe } from '../pipes/fill.pipe';
import { SearchPipe } from '../pipes/search.pipe';
import { ModalModule } from '../modals/modal.module';
import { MomentModule } from 'ngx-moment';

@NgModule({
	imports: [
	CommonModule,
	ModalModule,
	MomentModule
	],
	declarations: [
	FillPipe,
	SearchPipe,
	],
	exports: [
	FillPipe,
	SearchPipe,
	ModalModule,
	MomentModule
	]
})
export class SharedModule {}