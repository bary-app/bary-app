import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'dropdown',
  templateUrl: './dropdown.component.pug'
})

export class DropdownComponent {
  visible:boolean = false;

  @Input()
  customHTML:boolean;

  @Input()
  options: Object[];

  @Input()
  _label: String;

  @Input()
  value: Object;

  @Input()
  searchAttributes: string = '';

  @Output()
  valueChange: EventEmitter<Object>;

  @Output()
  onChange: EventEmitter<Object>;

  private query:string = "";
  private noBlur:boolean = false;

  constructor(private elementRef:ElementRef) {
    this.valueChange = new EventEmitter();
    this.onChange = new EventEmitter();
  }

  private getLabel(opt:Object):String{
    var label = '';

    if(opt !== null){
      label = this._label.replace(/%[\w]+%/g, function(key){
        key = key.replace(/%/g, "");

        return opt[key] || "";
      });
    }

    return label;
  };

  toggle(){
    this.visible = !this.visible;
  }

  select(value) {
    this.valueChange.emit(value);
    this.onChange.emit();
    this.visible = false;
  }

  private blured(){
    setTimeout(function () {
      if(!this.noBlur)
        this.visible=false;
    }.bind(this),100);
  }
}