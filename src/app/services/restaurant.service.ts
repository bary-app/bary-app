import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL } from '../app.values';

@Injectable()
export class RestaurantService{

	constructor(private http:HttpClient){}

	public get(id:number):Observable<any>{
		return this.http.get(API_URL+'restaurant/'+id, {});
	}

	public getFavourites(p?:number): Observable<any>{
		let page:number = 1;
		if(p)
			page = p;

		let params_obj:any = {
			'p': page
		};

		let params = new HttpParams({
			fromObject: params_obj
		});

		return this.http.get(API_URL+'user/favourites', {params: params});
	}

	public addToFavourites(id:number, value:boolean):Observable<any>{
		let url = (value) ? 'add' : 'remove';

		return this.http.post(API_URL+'restaurant/'+id+'/favourite/'+url, {});
	}

	public getAvailableReserveHours(id:number, date:string):Observable<any>{
		return this.http.get(API_URL+'restaurant/'+id+'/reserve/'+date, {});
	}

	public saveReserve(id:number, date:string, hour:string, num_pers:number, suggestions:string):Observable<any>{
		return this.http.post(API_URL+'restaurant/'+id+'/reserve/'+date, {
			"hour": hour,
			"num_pers": num_pers,
			"suggestions": suggestions
		});
	}

	public writeReview(id:number, rating:number, title?:string, description?:string):Observable<any>{
		return this.http.post(API_URL+'restaurant/'+id+'/review', {
			"rating": rating,
			"title": title,
			"description": description
		});
	}
}