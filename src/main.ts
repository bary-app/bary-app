import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import 'angular2-notifications';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
//import { environment } from './environments/environment.prod';

if (environment.production) {
	enableProdMode();
}

import { CityConfig } from './preload/cityconfig';

CityConfig.configure().then(() => {
	platformBrowserDynamic().bootstrapModule(AppModule).catch(err => console.log(err));
});